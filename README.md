# Orion

## General
For right time data access via NGSI v2 API. Orion Context Broker allows you to manage the entire lifecycle of context information including updates, queries, registrations and subscriptions. It is an NGSIv2 server implementation to manage context information and its availability. Using the Orion Context Broker, you are able to create context elements and manage them through updates and queries. In addition, you can subscribe to context information so when some condition occurs (e.g. the context elements have changed) you receive a notification.

Original documentation can be found here: https://fiware-orion.readthedocs.io

## Usage of Orion
Orion is a NGSI-V2 compliant Context Broker. It is used to receive JSON payloads and it will store the latest value to Mongo DB. These values can be queried.  
You can learn more here: https://fiware-orion.readthedocs.io/en/master/user/walkthrough_apiv2/index.html

Umbrella which acts as an PEP (Policy Enforcement Point), thus a correct Bearer token needs to be presented in the headers.  
You can find more information about Umbrella here: https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/umbrella

Orion also stores Subscriptions other entities / components have made. When it receives data, Orion will POST data to URL endpoints specified by subscription rules.

Or you can watch this Youtube-Video: https://www.youtube.com/watch?v=W_eJCJQ76BQ

## Versioning
Tested on:  
Version fiware/orion:2.3.0 (DockerHub digest: sha256:f2c2f6641b32dba0dec5cbff6a590e25a7fddba25f8a771794e90891e925d757)

## Volume usage
This component does not use persistent volumes.

## Load-Balancing
Currently only one Replica is deployed.

## Route (DNS)
Orion can be connected to via: https://context.fiware.opendata-CITY.de (No GUI available)

## Further Testing
The image is taken as is. No further component testing is done.

## Deployment
In order to deploy, following Gitlab-Variables and K8s-secrets are needed:
### Gitlab-Variables
KUBECONFIG - This variable contains the content of the Kube.cfg used to access the cluster for `dev`- and `staging`-stage.  
KUBECONFIG_PROD - This variable contains the content of the Kube.cfg used to access the cluster for `prod`-stage.  
NAMESPACE_DEV - This variable contains the namespace for dev-stage used by k8s  
NAMESPACE_STAGING - This variable contains the namespace for staging-stage used by k8s  
NAMESPACE_PROD - This variable contains the namespace for production-stage used by k8s
  
### Kubernetes Secrets
This project does not use Kubernetes secrets.

## Funding Information
The results of this project were developed on behalf of the city of Paderborn within the funding of the digital model region Ostwestfalen-Lippe of the state of North Rhine-Westphalia.

![img](img/logoleiste.JPG)

## License
Copyright © 2020 Profirator Oy, HYPERTEGRITY AG, omp computer gmbh

This work is licensed under the EUPL 1.2. See [LICENSE.md](LICENSE.md) for additional information.
